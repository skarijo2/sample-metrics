from sample_metrics import metrics, SampleClass


# Create a sample object and run its functions -------------------------------------------------------------------------
sample = SampleClass()
sample.sleepy_function()
sample.bad_function()
sample.sleepy_function()
sample.bad_function()
sample.bad_function()

# Print out the metrics ------------------------------------------------------------------------------------------------
print('\n=======METRICS=======')
for class_name in metrics:

    print('%s:' % class_name)

    for method in metrics[class_name]:

        invocations = metrics[class_name][method]['invocations']
        total_response_time = metrics[class_name][method]['total_response_time']
        average_response_time = round(total_response_time / invocations, 2)

        print('  %s ' % method)
        print('    Invocations: %s ' % invocations)
        print('    Average Response Time: %ss ' % average_response_time)
        print('    Exceptions:')

        for exception_name in metrics[class_name][method]['exceptions']:
            print('      {0}s: {1}'.format(exception_name, metrics[class_name][method]['exceptions'][exception_name]))

