import time
import random
from types import FunctionType
from functools import wraps


# Global configuration -------------------------------------------------------------------------------------------------
metrics = {}


# Wrapper function - does the work of calculating metrics --------------------------------------------------------------
def enable_metrics(class_name, method):

    method_name = method.__name__

    # Prepare the metrics dict for tracking this method
    if class_name not in metrics:
        metrics[class_name] = {}
    if method_name not in metrics[class_name]:
        metrics[class_name][method_name] = {'invocations': 0, 'total_response_time': 0, 'exceptions': {}}

    @wraps(method)
    def wrapped_function(*args, **kwargs):

        # Log an invocation of the method and start a timer
        metrics[class_name][method_name]['invocations'] += 1
        start_time = time.time()

        # Run the actual method
        try:
            result = method(*args, **kwargs)

        # Record metrics for any exceptions
        except Exception as ex:

            exception_name = ex.__class__.__name__

            if exception_name not in metrics[class_name][method_name]['exceptions']:
                metrics[class_name][method_name]['exceptions'][exception_name] = 1
            else:
                metrics[class_name][method_name]['exceptions'][exception_name] += 1

            # ..then raise the error as usual
            # raise - commented out to show the metrics
            return

        # Record the timed completion of the function
        finish_time = time.time()
        time_taken = finish_time - start_time
        metrics[class_name][method_name]['total_response_time'] += time_taken

        return result

    return wrapped_function


# Metaclass for tracking code performance metrics and bugs =============================================================
class ApplicationMetrics(type):

    # Handle creation of new class ------------------------------------------------------------------------------------
    def __new__(mcs, class_name, bases, class_dict):
        new_class_dict = {}

        # Loop through every attribute of the class. If it is a function, wrap it with the metric-tracking function
        for attributeName, attribute in class_dict.items():

            if isinstance(attribute, FunctionType):
                attribute = enable_metrics(class_name, attribute)

            new_class_dict[attributeName] = attribute

        return type.__new__(mcs, class_name, bases, new_class_dict)


# Sample class, shows the usage of the metaclass =======================================================================
class SampleClass(metaclass=ApplicationMetrics):

    def __init__(self):
        pass

    def sleepy_function(self):
        sleep_time = random.random()
        print('Sleeping for {0} seconds!'.format(sleep_time))
        time.sleep(sleep_time)

    def bad_function(self):
        print('Causing an exception.')
        print([][1])
